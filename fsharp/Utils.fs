﻿namespace FTW_HelloWorldOpen

open System
open System.IO

exception FtwException of string

module Constant =
    let mutable SERVER_URI = "testserver.helloworldopen.com"
    let mutable SERVER_PORT = "8091"
    let mutable BOT_NAME = "FTW"
    let BOT_COLOR = "Red"
    let mutable BOT_KEY = "k4yffsYHVeGzgQ"
    let LOG_FILENAME = "Logging.txt"

module Logger =
    /// Colored printf
    let cprintf c fmt = 
        Printf.kprintf 
            (fun s -> 
                let old = System.Console.ForegroundColor 
                try 
                  System.Console.ForegroundColor <- c;
                  System.Console.Write s
                finally
                  System.Console.ForegroundColor <- old) 
            fmt

    // Colored printfn
    let cprintfn c fmt = 
        cprintf c fmt |> ignore
        printfn ""

    let info (message : string) =
        let text = String.Format("{0}{1}", message, Environment.NewLine)
        File.AppendAllText(Constant.LOG_FILENAME, text)
        printf "%s" text

    let info2 (message : string) =
        let text = String.Format("{0}{1}", message, Environment.NewLine)
        File.AppendAllText(Constant.LOG_FILENAME, text)
        cprintf ConsoleColor.Yellow "%s" text

    let info3 (message : string) =
        let text = String.Format("{0}{1}", message, Environment.NewLine)
        File.AppendAllText(Constant.LOG_FILENAME, text)
        cprintf ConsoleColor.Cyan "%s" text

    let err (message : string) =
        let text = String.Format("ERROR: {0}{1}", message, Environment.NewLine)
        File.AppendAllText(Constant.LOG_FILENAME, text)
        cprintf ConsoleColor.Red "%s" text
﻿namespace FTW_HelloWorldOpen

open System
open FSharp.Data

type ServerMsgParser = JsonProvider<"./sample_msg_server.json", SampleIsList = true, RootName = "message">
type BotMsgParser = JsonProvider<"./sample_msg_bot.json", SampleIsList = true, RootName = "message">

type MsgTypeServer = 
    | YourCar of Car
    | GameInit of Race
    | GameStart
    | CarPositions of CarPosition list * Game
    | GameEnd of GameResult
    | TournamentEnd
    | Crash of Car * Game
    | Spawn of Car * Game
    | LapFinished of Car * LapResult * Game
    | Dnf of Car * string * Game
    | Finish of Car * Game

type MsgTypeBot = 
    | Join
    | SwitchLane
    | Throttle
    | Ping

/// <summary>
/// JsonReader will be responsible for translating json msg to corresponding class
/// </summary>
type JsonParser() = 
    
    static member ReadJson(message : string) = 
        let msgType = ServerMsgParser.Parse(message).MsgType
        match msgType with
        | "yourCar" -> 
            let car = 
                let data = ServerMsgParser.Parse(message).Data.Record.Value
                { Name = data.Name.Value
                  Color = data.Color.Value
                  Dimensions = None }
            MsgTypeServer.YourCar(car)
        | "gameInit" -> 
            let race = 
                let data = ServerMsgParser.Parse(message).Data.Record.Value
                { Track = 
                      let track = data.Race.Value.Track
                      { Id = track.Id
                        Name = track.Name
                        Pieces = 
                            let pieces = track.Pieces
                            pieces
                            |> Array.map (fun piece -> 
                                   if piece.Length.IsSome then 
                                       Piece.Straight({ Length = float piece.Length.Value
                                                        Switch = 
                                                            if piece.Switch.IsSome then piece.Switch.Value
                                                            else false
                                                        Bridge = 
                                                            if piece.Bridge.IsSome then Some true
                                                            else None })
                                   else 
                                       Piece.Bend({ Radius = piece.Radius.Value
                                                    Angle = float piece.Angle.Value }))
                            |> Array.toList
                        Lanes = 
                            let lanes = track.Lanes
                            lanes
                            |> Array.map (fun lane -> 
                                   { DistanceFromCenter = lane.DistanceFromCenter
                                     Index = lane.Index })
                            |> Array.toList
                        StartingPoint = 
                            let startingPoint = track.StartingPoint
                            { Position = 
                                  { X = float startingPoint.Position.X
                                    Y = float startingPoint.Position.Y }
                              Angle = float startingPoint.Angle } }
                  Cars = 
                      let cars = data.Race.Value.Cars
                      cars
                      |> Array.map (fun car -> 
                             { Name = car.Id.Name
                               Color = car.Id.Color
                               Dimensions = 
                                   Some { Length = float car.Dimensions.Length
                                          Width = float car.Dimensions.Width
                                          GuideFlagPosition = float car.Dimensions.GuideFlagPosition } })
                      |> Array.toList
                  RaceSession = 
                      { Laps = data.Race.Value.RaceSession.Laps
                        MaxLapTimeMs = int64 data.Race.Value.RaceSession.MaxLapTimeMs
                        QuickRace = data.Race.Value.RaceSession.QuickRace } }
            MsgTypeServer.GameInit(race)
        | "gameStart" -> MsgTypeServer.GameStart
        | "carPositions" -> 
            let data = ServerMsgParser.Parse(message)
            
            let carPositions = 
                data.Data.Array.Value
                |> Array.map (fun carPosition -> 
                       { Car = 
                             { Name = carPosition.Id.Name
                               Color = carPosition.Id.Color
                               Dimensions = None }
                         Angle = float carPosition.Angle
                         PiecePosition = 
                             { PieceIndex = carPosition.PiecePosition.PieceIndex
                               InPieceDistance = float carPosition.PiecePosition.InPieceDistance
                               Lane = 
                                   carPosition.PiecePosition.Lane.StartLaneIndex, 
                                   carPosition.PiecePosition.Lane.EndLaneIndex
                               Lap = carPosition.PiecePosition.Lap } })
                |> Array.toList
            
            let game = 
                { Id = data.GameId.Value.ToString()
                  Tick = data.GameTick  }
            
            MsgTypeServer.CarPositions(carPositions, game)
        | "gameEnd" -> 
            let data = ServerMsgParser.Parse(message).Data.Record.Value
            
            let gameResult = 
                { Results = 
                      data.Results
                      |> Array.map (fun item -> 
                             let car = 
                                 { Name = item.Car.Name
                                   Color = item.Car.Color
                                   Dimensions = None }
                             
                             let result = 
                                 { Laps = item.Result.Laps
                                   Ticks = item.Result.Ticks
                                   Millis = Convert.ToInt64 (item.Result.Millis) }
                             
                             car, result)
                      |> Array.toList
                  BestLaps = 
                      data.BestLaps
                      |> Array.map (fun item -> 
                             let car = 
                                 { Name = item.Car.Name
                                   Color = item.Car.Name
                                   Dimensions = None }
                             
                             let result = 
                                 { Lap = item.Result.Lap
                                   Ticks = item.Result.Ticks
                                   Millis = Convert.ToInt64 (item.Result.Millis) }
                             
                             car, result)
                      |> Array.toList }
            MsgTypeServer.GameEnd(gameResult)
        | "tournamentEnd" -> MsgTypeServer.TournamentEnd
        | "crash" -> 
            let data = ServerMsgParser.Parse(message)
            
            let car = 
                { Name = data.Data.Record.Value.Name.Value
                  Color = data.Data.Record.Value.Color.Value
                  Dimensions = None }
            
            let game = 
                { Id = data.GameId.Value.ToString()
                  Tick = data.GameTick }
            
            MsgTypeServer.Crash(car, game)
        | "spawn" -> 
            let data = ServerMsgParser.Parse(message)
            
            let car = 
                { Name = data.Data.Record.Value.Name.Value
                  Color = data.Data.Record.Value.Color.Value
                  Dimensions = None }
            
            let game = 
                { Id = data.GameId.Value.ToString()
                  Tick = data.GameTick }
            
            MsgTypeServer.Spawn(car, game)
        | "lapFinished" -> 
            let data = ServerMsgParser.Parse(message)
            
            let car =
                { Name = data.Data.Record.Value.Car.Value.Name
                  Color = data.Data.Record.Value.Car.Value.Color
                  Dimensions = None }

            let lapResult =
                { LapTime = 
                      { Lap = data.Data.Record.Value.LapTime.Value.Lap
                        Ticks = data.Data.Record.Value.LapTime.Value.Ticks
                        Millis = Convert.ToInt64 (data.Data.Record.Value.LapTime.Value.Millis) }
                  RaceTime = 
                      { Laps = data.Data.Record.Value.RaceTime.Value.Laps
                        Ticks = data.Data.Record.Value.RaceTime.Value.Ticks
                        Millis = Convert.ToInt64 (data.Data.Record.Value.RaceTime.Value.Millis) }
                  Ranking = 
                      { Overall = data.Data.Record.Value.Ranking.Value.Overall
                        FastestLap = data.Data.Record.Value.Ranking.Value.FastestLap } }
            let game = 
                { Id = data.GameId.Value.ToString()
                  Tick = data.GameTick }
            MsgTypeServer.LapFinished(car, lapResult, game)
        | "dnf" -> 
            let data = ServerMsgParser.Parse(message)
            
            let car = 
                { Name = data.Data.Record.Value.Name.Value
                  Color = data.Data.Record.Value.Color.Value
                  Dimensions = None }
            
            let reason = data.Data.Record.Value.Reason.Value
            
            let game = 
                { Id = data.GameId.Value.ToString()
                  Tick = data.GameTick }
            MsgTypeServer.Dnf(car, reason, game)
        | "finish" -> 
            let data = ServerMsgParser.Parse(message)
            
            let car = 
                { Name = data.Data.Record.Value.Name.Value
                  Color = data.Data.Record.Value.Color.Value
                  Dimensions = None }
            
            let game = 
                { Id = data.GameId.Value.ToString()
                  Tick = data.GameTick }
            
            MsgTypeServer.Finish(car, game)
        | _ -> raise <| FtwException("Unknown msgType")
    
    static member WriteJson (msgType : MsgTypeBot) (data : string []) = 
        let exMsg = "Wrong data when creating json"
        match msgType with
        | Join -> 
            if data.Length <> 2 then raise <| FtwException(exMsg)
            let content = BotMsgParser.DecimalOrStringOrData(record = BotMsgParser.Data(name = data.[0], key = data.[1]))
            BotMsgParser.Message(msgType = "join", data = content, gameTick = None).JsonValue
        | SwitchLane -> 
            if data.Length <> 1 then raise <| FtwException(exMsg)
            let content = BotMsgParser.DecimalOrStringOrData(string = data.[0])
            BotMsgParser.Message(msgType = "switchLane", data = content, gameTick = None).JsonValue
        | Throttle -> 
            if data.Length <> 1 && data.Length <> 2 then raise <| FtwException(exMsg)
            let content = BotMsgParser.DecimalOrStringOrData(number = Convert.ToDecimal(data.[0]))
            let tick =
                if data.Length = 1 then
                    None
                elif data.Length = 2 then
                    Some (int data.[1])
                else
                    None
            BotMsgParser.Message(msgType = "throttle", data = content, gameTick = tick).JsonValue
        | Ping ->
            let content = BotMsgParser.DecimalOrStringOrData()
            BotMsgParser.Message(msgType = "ping", data = content, gameTick = None).JsonValue
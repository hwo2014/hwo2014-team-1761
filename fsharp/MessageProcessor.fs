﻿namespace FTW_HelloWorldOpen

open System
open System.Collections.Generic

type MessageProcessor () =

    let _messageQueue = new Queue<string>()

    member x.MessageQueue
        with get () =
            let rec retry () = 
                try
                    _messageQueue.Dequeue()
                with
                    | :? InvalidOperationException as ex -> 
                        System.Threading.Thread.Sleep(100)
                        retry()
            retry()
                    
        and set newMsg =
            _messageQueue.Enqueue(newMsg)
            

    static member ReadMessage (message : string) =
        JsonParser.ReadJson(message)

    static member ProcessMessage (messageType : MsgTypeServer) =
        match messageType with
        | YourCar car ->
            ()
        | GameInit race ->
            ()
        | _ ->
            ()

    static member CreateMessage (msgType : MsgTypeBot) (data : string[]) =
        JsonParser.WriteJson msgType data

    static member Ping =
        MessageProcessor.CreateMessage MsgTypeBot.Ping Array.empty

    static member Throttle (value : float) (tick : int option) =
        let param =
            if tick.IsSome then [| value.ToString(); tick.Value.ToString() |]
            else [| value.ToString() |]
        MessageProcessor.CreateMessage MsgTypeBot.Throttle param

    static member SwitchLane (direction : SwitchDirection) =
        let directionStr =
            match direction with 
            | Left ->
                "left"
            | Right ->
                "right"
        MessageProcessor.CreateMessage MsgTypeBot.SwitchLane [| directionStr |]

    static member Join (botName : string) (botKey : string) =
        MessageProcessor.CreateMessage MsgTypeBot.Join [| botName; botKey |]
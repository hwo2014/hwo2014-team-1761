﻿namespace FTW_HelloWorldOpen

type StateType =
    | NotInit
    | Init
    | ServerAck
    | GameReady
    | GameStarted
    | GameInProgress
    | GameEnd
    | Exit

type State () =
    let mutable _state = StateType.NotInit 
    let mutable _game = 
        { Id = ""
          Tick = None }

    member x.State 
        with get () = 
            _state
        and set value = 
            let oldValue = _state
            if oldValue <> value then
                _state <- value
                Logger.info3 <| sprintf "New STATE: %A" _state

    member x.Game
        with get () =
            _game
        and set value =
            let oldValue = _game
            if value.Tick.IsSome then
                _game <- value

    member val MyCar =
        { Name = ""
          Color = ""
          Dimensions = None }
        with get, set

    member val Race =
        { Track =
            { Id = ""
              Name = ""
              Pieces = []
              Lanes = []
              StartingPoint =
                { Position =
                    { X = 0.
                      Y = 0.}
                  Angle = 0. } }
          Cars = []
          RaceSession =
            { Laps = 0
              MaxLapTimeMs = int64 0
              QuickRace = true } }
        with get, set

    member val CarPositions = 
        [ { Car =
                { Name = ""
                  Color = ""
                  Dimensions = None }
            Angle = 0.
            PiecePosition =
                { PieceIndex = 0
                  InPieceDistance = 0.0
                  Lane = 0, 0 
                  Lap = 0 } } ] with get, set

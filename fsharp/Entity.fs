﻿namespace FTW_HelloWorldOpen

type CarDimension = 
    { Length : float
      Width : float
      GuideFlagPosition : float }

type Car = 
    { Name : string
      Color : string
      Dimensions : CarDimension option }

type PiecePosition = 
    { PieceIndex : int
      InPieceDistance : float
      Lane : int * int
      Lap : int }

type CarPosition = 
    { Car : Car
      Angle : float
      PiecePosition : PiecePosition }

type StraightPiece = 
    { Length : float
      Switch : bool
      Bridge : bool option }

type BendPiece = 
    { Radius : int
      Angle : float }

type Piece = 
    | Straight of StraightPiece
    | Bend of BendPiece

type Lane = 
    { DistanceFromCenter : int
      Index : int }

type Point = 
    { X : float
      Y : float }

type StartingPoint = 
    { Position : Point
      Angle : float }

type Track = 
    { Id : string
      Name : string
      Pieces : Piece list
      Lanes : Lane list
      StartingPoint : StartingPoint }

type Session = 
    { Laps : int
      MaxLapTimeMs : int64
      QuickRace : bool }

type Race = 
    { Track : Track
      Cars : Car list
      RaceSession : Session }

type Game = 
    { Id : string
      Tick : int option }

type Lap = 
    { Lap : int
      Ticks : int
      Millis : int64 }

type Rank = 
    { Overall : int
      FastestLap : int }

type Result = 
    { Laps : int
      Ticks : int
      Millis : int64 }

type LapResult = 
    { LapTime : Lap
      RaceTime : Result
      Ranking : Rank }

type BestLap = 
    { Lap : int
      Ticks : int
      Millis : int64 }

type GameResult = 
    { Results : (Car * Result) list
      BestLaps : (Car * BestLap) list }

type SwitchDirection = 
    | Left
    | Right

type Throttle = 
    { Data : float
      GameTick : int }
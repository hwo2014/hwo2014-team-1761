﻿open FTW_HelloWorldOpen

let test () =
//    let msg = """ {"msgType":"carPositions","data":[{"id":{"name":"FTW","color":"red"},"angle":0.04310317192297389,"piecePosition":{"pieceIndex":0,"inPieceDistance":5.756624548256269,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":3}}],"gameId":"fdf2c11e-3941-4021-bba9-8383e216fd65"} """
//
//    let controller = Controller()
//    controller.SetState(StateType.GameInProgress)
//    controller.Respond(msg) |> ignore
    ()

open System
open System.Net.Sockets

[<EntryPoint>]
let main args =
    if args.Length > 0 then
        let (host, portString, botName, botKey) = 
            args |> (function [|a;b;c;d|] -> a, b, c, d | _ -> failwith "Invalid param array")

        Constant.SERVER_URI <- host
        Constant.SERVER_PORT <- portString
        Constant.BOT_NAME <- botName
        Constant.BOT_KEY <- botKey

    use client = new TcpClient(Constant.SERVER_URI, int Constant.SERVER_PORT)
    let communicator = ServerCommunicator(client)
    let controller = Controller()
    Logger.info "===================================================================="
    Logger.info <| sprintf "Connecting to %s:%s as %s/%s at %s" 
                    Constant.SERVER_URI Constant.SERVER_PORT Constant.BOT_NAME Constant.BOT_KEY (DateTime.Now.ToString())
    communicator.Send <| controller.Init()

    communicator.Loop (controller.Respond)
    System.Console.ReadLine() |> ignore

    0


﻿namespace FTW_HelloWorldOpen

open System
open System.IO
open System.Net.Sockets
open FSharp.Data

type ServerCommunicator (client : TcpClient)  =

    let mutable retryCount = 10

    let stream = client.GetStream()
    let reader = new StreamReader(stream)
    let writer = new StreamWriter(stream)

    member this.Send (msg : JsonValue) =
        let msgText = msg.ToString(JsonSaveOptions.DisableFormatting)
        Logger.info2 (sprintf "CLIENT: %s" msgText)
        writer.WriteLine msgText
        writer.Flush()

    member this.Loop responder=
        let processMsg msg =
            match msg with
            | null -> 
                Logger.err "Null message from server"
                retryCount <- retryCount - 1
                if retryCount <= 0 then
                    Logger.err "Exiting after 10 retries ..."
                    false
                else
                    Logger.err "Retrying ..."
                    Threading.Thread.Sleep(200)
                    true
            | line ->
                try
                    retryCount <- 10
                    Logger.info <| sprintf "SERVER: %s" msg
                    let respond = responder msg
                    this.Send <| respond
                    true
                with
                | FtwException ex -> 
                    Logger.err ex
                    true
                | ex ->
                    Logger.err ex.Message
                    true

        while processMsg (reader.ReadLine()) do 
            ()
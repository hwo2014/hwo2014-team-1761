﻿namespace FTW_HelloWorldOpen

type Server () =
    static member ReceiveMessage () =
        ""
    static member SendMessage (msg : string) =
        ()

type Controller () =
    let _state = State()
    let _messages = MessageProcessor()

    // DEBUG ONLY
    member x.SetState (s : StateType) =
        _state.State <- s

    member x.Init () =
        match _state.State with
        | NotInit ->
            _state.State <- StateType.Init
            MessageProcessor.Join Constant.BOT_NAME Constant.BOT_KEY
        | _ ->
            raise <| FtwException("Controller is already initialized")

    member x.Respond (msg : string) =
        _messages.MessageQueue <- msg
        match _state.State with
        | NotInit ->
            MessageProcessor.Ping
        | Init ->
            let msg = _messages.MessageQueue
            let msgContent = MessageProcessor.ReadMessage(msg)
            match msgContent with
            | YourCar car ->
                _state.State <- StateType.ServerAck
                _state.MyCar <- car
            | _ ->
                ()
            MessageProcessor.Ping
        | ServerAck ->
            let msg = _messages.MessageQueue
            let msgContent = MessageProcessor.ReadMessage(msg)
            match msgContent with
            | GameInit race ->
                _state.Race <- race
                _state.State <- StateType.GameReady
            | _ ->
                ()
            MessageProcessor.Ping
        | GameReady ->
            let msg = _messages.MessageQueue
            let msgContent = MessageProcessor.ReadMessage(msg)
            match msgContent with
            | GameStart ->
                _state.State <- StateType.GameStarted
            | _ ->
                ()
            MessageProcessor.Ping
        | GameStarted ->
            let msg = _messages.MessageQueue
            let msgContent = MessageProcessor.ReadMessage(msg)
            match msgContent with
            | CarPositions (carPositions, game) ->
                _state.State <- StateType.GameInProgress
                _state.CarPositions <- carPositions
                _state.Game <- game
            | _ ->
                ()
            MessageProcessor.Ping
        | GameInProgress ->
            let msg = _messages.MessageQueue
            let msgContent = MessageProcessor.ReadMessage(msg)
            match msgContent with
            | CarPositions (carPositions, game) ->
                _state.State <- StateType.GameInProgress
                _state.CarPositions <- carPositions
                _state.Game <- game
                AlgoExecutor.Operate msgContent _state
            | Crash (car, game) ->
                _state.Game <- game
                AlgoExecutor.Operate msgContent _state
            | Spawn (car, game) ->
                _state.Game <- game
                AlgoExecutor.Operate msgContent _state
            | LapFinished (car, lapResult, game) ->
                _state.Game <- game
                Logger.info3 <| sprintf "%s finish lap %d" car.Name lapResult.LapTime.Lap
                AlgoExecutor.Operate msgContent _state
            | Dnf (car, reason, game) ->
                _state.Game <- game
                AlgoExecutor.Operate msgContent _state
            | Finish (car, game) ->
                _state.Game <- game
                AlgoExecutor.Operate msgContent _state
            | GameEnd gameResult ->
                _state.State <- StateType.GameEnd
//                () //TODO: Show result when game ends
                MessageProcessor.Ping
            | _ ->
                MessageProcessor.Ping
        | StateType.GameEnd ->
            let msg = _messages.MessageQueue
            let msgContent = MessageProcessor.ReadMessage(msg)
            match msgContent with
            | TournamentEnd ->
                _state.State <- Exit
            | GameStart ->
                _state.State <- StateType.GameStarted
                //TODO: Game restart with qualifying results
            | _ ->
                ()
            MessageProcessor.Ping
        | Exit ->
            //TODO: Show result overall
            MessageProcessor.Ping
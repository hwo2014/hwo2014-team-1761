﻿namespace FTW_HelloWorldOpen

type Algo1 () =

    static member MyCarAngle (carPositions : CarPosition list) =
        let myPosition = carPositions |> List.find(fun e -> e.Car.Name = Constant.BOT_NAME)
        myPosition.Angle

    static member Operate (message : MsgTypeServer) (state : State) =
        match message with
        | CarPositions (carPositions, game) ->
            let curPosition = carPositions |> List.find(fun e -> e.Car = state.MyCar)
            let curPieceIndex = curPosition.PiecePosition.PieceIndex
            let straightPiece = Some
            let curPiece = List.nth state.Race.Track.Pieces curPieceIndex
            let curAngle = curPosition.Angle    

            if System.Math.Abs (Algo1.MyCarAngle carPositions) > 1. then
                MessageProcessor.Throttle 0.4 game.Tick
            else
                MessageProcessor.Throttle 0.8 game.Tick
        | _ ->
            MessageProcessor.Ping

type AlgoExecutor () =

    static member Operate (message: MsgTypeServer) (state : State) =
        Algo1.Operate message state
